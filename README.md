# inventory management
Simple application to create, read, update and delete product information, and file uploading using Node.js , Angularjs-1.6

1. Open git terminal (to pull the code)

2. cd inventory (type this in git terminal switch to inventory)

3. npm install (type this in the git terminal and install all the node modules)

4. Create your local mongo db and replace your db name in app.js file and save it.

5. node app.js (type this in the git terminal to start the server)

6. Run the app in your web browser http://localhost:3000